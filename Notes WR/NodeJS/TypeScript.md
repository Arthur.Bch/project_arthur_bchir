# ___TypeScript et NodeJS___

NodeJs est un moteur d'exécution de JavaScript, c'est un langage qui est orienté serveur. On retrouve la documentation
de nodeJS à ce [lien](https://nodejs.org/dist/latest-v14.x/docs/api/).

Npm est un site qui recense des millions de codes utiles à différents usages, des gros frameworks comme Angular ou React
mais aussi de plus petites librairies. Le site officiel est trouvable à ce
[lien](https://www.npmjs.com/).

[TypeScript](https://www.typescriptlang.org/) est un langage OpenSource ajoutant des fonctionnalités à JS. Il améliore
le code JS vanilla, notamment en sécurité et en lisibilité. Pour transformer un code en TypeScript en JavaScript, qui
sera lisible par les navigateurs à l'inverse d'un fichier .ts (typescript), on devra utiliser un transpiller. Pour
transpiller un fichier TS en JS, on passe par la console ou le terminal et on fait précéder le nom du fichier par ___"
tsc"___. Cela aura pour effet de créer un fichier .js a partir du fichier .ts

On peut créer des paquets pour les partager. Il faut pour cela commencer par taper __"npm init"__ dans le terminal.
Après avoir rentré les informations demandées, un fichier va être créé automatiquement par npm, il aura pour nom "
package.json". On peut utiliser des librairies dans notre code, pour cela il faut les installer, par exemple, pour
installer express, il faut utiliser dans le terminal, dans le dossier qui contient notre projet, la commande __"npm i
express"__. Cela récupère le code de la librairie en question (ici express).

Ajouter des librairies va ajouter un ligne dans notre fichier __"package.json"__, la ligne "dependencies" : {}  
Celle ci contiendra toutes les dépendances que l'on utilise dans notre paquet. Cela va aussi avoir pour conséquence de
créer un nouveau fichier : __"package-lock.json"__.  
Celui-ci contiendra les dépendances de la librairie dont dépend notre paquet.

Un nouveau dossier va aussi etre créé, le dossier __"node_modules"__, il possède toutes les dépendances en fichiers.

A noter que lorsqu'on partage un projet, on supprime ce dossier ou on l'enlève de la liste de partage, étant donné son
poids important !  
==> La personne qui récupère notre paquet n'aura qu'à faire dans son terminal la commande __"npm install"__, pour
récupérer toutes les dépendences et donc le dossier __"node_modules"__.

Une autre chose importante dans le fichier __package.json__ est la ligne : __"scripts": { },__  
Elle contiendra les scripts exécutables via le terminal  
==> pour l'exécuter, il faut taper la commande __"npm run nomDuScriptAExecuter"__.

## _Le typage_

Avec TS, il est possible de donner un type a une variable :

    let num: number = 3;

Il existe de nombreux typages pour chaque type d'élément JS, on les retrouve dans la documentation
de [TS](https://www.typescriptlang.org/docs/handbook/basic-types.html).

## _Les fonctions_

L'avantage de TS pour les fonctions est la possibilité de typer la fonction, càd déterminer le type des arguments, de la
valeur de retour, il est meme possible de typer une variable par une fonction.

```typescript
let multiply: (num1: number, num2: number) => number; // on définit une variable multiply comme un type fonction avec 
// des arguments de type number et une valeur de retour de type number.
multiply = function (num1, num2) {
    return num1 * num2;
}
```

Différentes informations à ce propos sont retrouvables sur le site officiel
de [TS](https://www.typescriptlang.org/docs/handbook/functions.html).

## _Les énumérateurs (enum)_

Les [enums](https://www.typescriptlang.org/docs/handbook/enums.html) donnent la possibilité de déterminer un set de
constantes nommées. Elles peuvent etre numériques ou bien chaines de charactères.

## _Les tuples_

Les [tuples](https://www.typescriptlang.org/docs/handbook/basic-types.html#tuple) permettent de définir un array avec un
nombre défini d'éléments, pour lesquels le type est défini.

## _Les types littéraux_

Les types littéraux ou literal types permettent de donner des valeurs possibles à une variable, cette dernière devra
respecter cette déclaration.

    let num123: 1 | 2 | 3; // On définit une variable, elle ne pourra prendre que 1, 2 ou 3 comme valeurs.
    num123 = 1; // On la définit à 1, cela fonctionne;
    num123 = 4; // Ici ce n'est pas possible de lui donner la valeur 4 car ce n'est pas autorisé dans la définition de type.

## _Types assertions_

Il est possible de réassigner le type d'une variable pour pouvoir utiliser une variable via le mot clé __as__.

    let input: unknown = "24";
    let var1: number;
    var1 = (input as number); // On dit au navigateur par ce biais que l'input va etre de type number
    var1 = (input as any); // Si on utilise "any", la variable input peut etre de tout type.

## _Les interfaces_

Les [interfaces](https://www.typescriptlang.org/docs/handbook/interfaces.html) ressemblent aux classes, leur role est de
s'assurer de la structure d'objets créés à partir d'elles memes. Ainsi, on va définir, comme pour les classes, un modèle
pour la création de nouveaux objets mais ici, on aura un controle sur le type des parametres et des méthodes. Les
interfaces sont définies comme suit :

```typescript
interface ISquareConfig {
    color?: string;
    width?: number;
}
// écrire la key avec ?: la définit comme étant optionnelle, on peut ne pas la définir lors de la création d'un objet ici

function createSquare(config: ISquareConfig): { color: string; area: number } {
    // ici, on ajoute une nouvelle propriété area dans la fonction, et on lui done le type number
    let newSquare = {color: "white", area: 100}; // On définit un objet newSquare avec des propriétés par défaut
    if (config.color) {
        newSquare.color = config.color;
    } 
    // ici, on dit que si la fonction est appelée avec un couleur en argument, notre objet newSquare aura la couleur 
    // donnée en argument et non plus celle par défaut
    if (config.width) {
        newSquare.area = config.width * config.width;
    } 
    // pareil pour l'arg area
    return newSquare;
}

let mySquare = createSquare({color: "black"});
let mySquare2 = createSquare(({area: 200}));
// On aura donc ici un nouvel objet avec comme couleur "black" et comme area 100;
```

## _Les classes_

### _Définition de classes et implementation via Interfaces_ 

Les classes peuvent êtres créés à partir d'interfaces, elles doivent dans ce cas respecter le contrat définit par l'interface
et sont définies avec le mot clé __"implements"__ : 

    class Car implements ICar { };

### _Classes extends par classes parentes_

Il en va de meme pour les classes et les extensions de classes.

```typescript
class Animal {
name: string;
constructor(theName: string) {
this.name = theName;
}
move(distanceInMeters: number = 0) {
console.log(`${this.name} moved ${distanceInMeters}m.`);
}
}

class Snake extends Animal {
constructor(name: string) {
super(name);
}
move(distanceInMeters = 5) {
console.log("Slithering...");
super.move(distanceInMeters);
}
}

class Horse extends Animal {
constructor(name: string) {
super(name);
}
move(distanceInMeters = 45) {
console.log("Galloping...");
super.move(distanceInMeters);
}
}

let sam = new Snake("Sammy the Python");
let tom: Animal = new Horse("Tommy the Palomino");

sam.move();
tom.move(34);
```

### _Niveau de protection des propriétés ou méthodes d'une classe_

Il est possible de définir 3 niveaux de protections des parametres et des méthodes : __public__ (par défaut), 
__private__ ou __protected__.
Une variable en "public" pourra etre accédée partout.
En "private", elle ne sera accessible que dans le scope dans lequel elle se trouve.
En "protected", on pourra y accéder dans le parents et les enfants uniquement.*

On notera que pour typer une structure, on utilise une interface et non une classe ! En effet, l'interface existant 
uniquement dans TS et pas dans JS, elle sera supprimée lors de la compilation, entrainant un gain de performances.

### _Propriétés des paramètres propres à TS_

On va pouvoir gagner du temps et des lignes en définissant une propriété et en l'assignant en meme temps dans le 
constructor d'une classe nos paramètres grace à TS. 

```typescript
class Person {
    name: string; // inutile grace a la définition dans le constructor
    age: number; // pareil 
    
    constructor(public name: string, public age: number) {
        this.name = name; // aussi inutile grace à la définition et assignation dans le constructor
        this.age = age; // pareil ! 
    }
}

let person: Person = new Person("Jannot", 22);
```

## _Import et export modules_ 

Il est possible de créer des modules dans un fichier qui seront, une fois importés dans un autre fichier, utilisables
dans ce dernier. Cela permet de décentraliser un code et de le rendre plus léger. 
Pour cela, il faut rendre les éléments que l'on veux exporter exportables, on utilise le mot clé __"export"__ devant 
l'élément à exporter ou bien en faisant un condensé de tous les éléments. Ex: 

    export {Car, ICar}; // on exporte un objet composé de la classe Car et l'interface ICar;
    export default Car;

On peut importer les éléments de différentes manières, via le mot clé __"import"__. 

    import * as fromCar from "./car"; 
    // ici, on import tous les exports venant du fichier "car" et on les place dans une variable "fromCar"
    let car = new fromCar.Car('RS3', 1313);
    // On définit un nouvel objet car via la class Car définie dans l'autre fichier en utilisant notre nouvelle variable
    // fromCar.Car 

Il est aussi possible de ne pas récupérer l'ensemble de ce qui est exporté.

    import {Car as carClass} from "./car";
    let car = new carClass('RS3', 1313);
    // On a donc importé uniquement la class Car et on l'a renommée en carClass

## _TS configs_ 

tsc -h => afficher l'aide dans la console 
tsc --init => créer un fichier tsconfig.json ou toutes les configurations du fichier sont présentes, elles sont aussi 
modifiables pour personnaliser la compilation !
