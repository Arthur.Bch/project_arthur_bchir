# _Les objets_

Les [objets](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Object) en JavaScript sont composés de propriétés que l'on sépare en deux parties, la clé et la valeur et de méthodes.
Les propriétés le définissent et les méthodes sont faites pour l'utiliser.

```javascript
    // object = phone ici
    let phone = {
    //propriétés
        //key: value,
        name: "Note10",
        marque: "Samsung",
        poids: "400g",
    //méthodes
        call: function () {
        console.log("call Sam")
        },
        sendMsg: function (to) {
        console.log("Send msg to" + to)
        }
    };

// afficher dans la console les propriétés et les méthodes de l'objet phone 
console.log(phone);
// afficher la marque en particulier
console.log("marque : ", phone.marque);
//appeler une des méthodes de l'objet phone
phone.sendMsg("Jean");

    let phone2 = {
        marque: "Huawei"
    };
    phone2.largeur = 6;
    let nomPropriété = "poids";
    console.log(phone2.largeur, phone[nomPropriété], phone["marque"], phone2["marque"]);
```

## _Accéder à des objets courants dans une méthode_

Il est possible d'accéder à un objet courant avec le terme __this__. On peut donc accéder aux paramètres d'un objet dans
la méthode d'un meme objet grace au terme __this__.

```javascript
let student = {
    name: "Arthuro",
    age: 28,
    class: "chômage",
    motivation: "up to space",
    says: function(text_à_dire) {
        console.log(this.name + ": " + text_à_dire); 
        //ici, on écrit this.name ==> le nom de l'objet actuel : student ==> Arthuro: text à dire 
    },
};
```

On peut aussi s'en servir pour accéder a des informations dans les paramètres de l'objet et les modifier. On entre une
autre value à une clé d'un paramètre en utilisant une méthode de l'objet avec le __this__.

```javascript
let student2 = {
    name: "Rudyx",
    age: 25,
    class: "se touche",
    changeClass: function(className) {
        this.class = className;
        //ici, on change la classe qui était "se touche" par un nouveau nom qu'on définira en argument lors de l'appel
        //de la fonction changeClass
    }
}; 
student2.changeClass("White Rabbit");
console.log("Bonjour, je suis actuellement en formation chez " + student2.class);
```

## _Méthodes JS sympas et utiles_

### _Le spread operator (...)_

Le spread operator (qui s'écrit "...") permet d'étaler ou de copier des données de différentes structures comme des
arrays ou des objets.
Il est très pratique et utilisé, notamment pour créer de nouveaux objets qui prendront des propriétés d'un objet type
du même genre, permettant après de redéfinir les données à changer en gardant les données de bases similaires aux
différents objets.  
__//!\\ En JS, on doit passer par cette méthode, car si on essaye de "copier" un objet ou autre type de donnée en faisant :  
let orange = fruit ==> cela copierait la référence et tout changement opèrerait sur les deux objets ce qui n'est pas le
but ! //!\\__

```javascript
let fruit = {
    type: "fruit",
    fonction: "nourir",
    opposé: "legumes",
};

let orange = {
    ...fruit,
    name: orange,
};
```

### _Affectation par décomposition_

L'affectation par décomposition permet de décomposer un objet pour affecter ses composantes
à d'autres données, par ex des variables.

```javascript
let student = {name: "John", age: 20, classe: "B"};

let name, classe;
({name, class} = student); // les () servent à "dire à JS" que les {} ne sont pas un bloc d'instruction 
// ici, on récupère la variable age appartenant à l'objet student, on doit la déclarer avant et on doit utiliser le nom
//de la clé utilisée dans l'objet

const {name, age} = student; 
// ici meme chose mais on fait l'affectation en meme temps que la déclaration (et on les définit comme des constantes)

let {name, classe: niveau = "A"} = student; 
// en faisant "class: niveau", on redéfinit le nom de la variable en meme temps qu'on l'affecte
// donc, on reprend la key class de student et on renomme notre variable niveau, cela ne change pas la key dans l'objet
// le "niveau = "A" correspond à un changement de la valeur par défaut dans le cas ou la key "class" n'existe pas dans
//notre objet student. Si elle existe, niveau sera égal à class de student donc "B".
```

