# LES STRINGS, LES STRUCTURES CONDITIONNELLES, ET LES LOOPS

## _Les strings_

Les strings sont des chaînes de caractères, elles peuvent être définies dans des variables ou des constantes et doivent
être entourées par des guillemets (" ", ' ', ``).

Attention : si elles possèdent à l'intérieur le même élément que celui utilisé pour la délimiter, il faut l'échapper en
utilisant un \ devant.

```javascript
let str = "Bien le bonjour, jeune panda d'ailleurs."
let str2 = "\"Bonjour\" répondit-il d'une voix douce.";
```

Pour "échapper" un \\, il faut en ajouter un 2e \ devant.  
__ATTENTION : MD met systématiquement un double\ ,il n'y en a qu'un en réalité!!!!__

### _Concaténation_

Il est possible de concaténer les strings ensemble en utilisant le signe +.

### _Méthodes_ (Methods)

Il existe de nombreuses fonctions JS pour manipuler les strings, on peut les retrouver sur la documentation officielle
de [Mozilla](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String).


## _Les structures conditionnelles_

Il est possible de checker des conditions dans le cas ou on veut obtenir deux comportements différents en fonction de 
différentes conditions préalables. 

### _Le if, else if, else_

Cette structure permet de vérifier qu'une condition est établie avant de passer dans un morceau de code. On retrouve donc
le __if__ en première position suivi de la __condition__, puis le code qu'on veut mettre si la condition est vraie. Viens 
ensuite, éventuellement un __else if__ pour checker une autre condition, et enfin le __else__ avec un code différent. Le fait est 
que le code ne passera que dans une condition.

```javascript
let name = "John";
if (name === "John") {
    console.log("Bonjour " + name + " !");
}
else if (name === "Richard") {
    console.log("Bonjour " + name + " !");
}
else {
    console.log("Bonjour inconnu !");
}
```

### _Le ternaire_
  
Le __ternaire__ est une facon d'écrire de manière plus compressée les structures conditionnelles, entre autres. 
On peut ainsi écrire la condition if else en une ligne : 
```javascript
let name2 = "Dave";
console.log((name === "Dave") ? "Bonjour " + name + " !" : "Bonjour inconnu !");
```

### _Le switch case_

Lors de cas ou il y a de nombreuses conditions if, else if, else if, else if ... il peut etre intéressant d'utiliser le
__switch__. 

```javascript
let fruit = "banane";
switch (fruit) {
    case "pommes" : {
        let message = " Les pommes coûtent 2€ le kg.";
        console.log(message);
        break;
    }
    
    case "bananes" : {
        let message = " Les bananes coûtent 1.50€ le kg.";
        console.log(message);
        break;
    }
    
    case "cerises" :
    case "fraises" :
        console.log("Les cerises et les fraises coûtent 4€ le kg");
        break;
        
    default : 
        console.log("Ceci n'est pas un fruit");
        break;
}
```

## _Les loops_

### _Le for loop_

Les boucles servent à faire tourner un code plusieurs fois avant de passer à la suite des instructions. La boucle for est
utilisée préférentiellement quand on connait les conditions d'entrée et de sortie de boucle. Elle se présente de la manière
suivante:

```javascript
let arr = ["salut", "les", "nuls"];

for (let i = 0; i < arr.lenght; i++) {
    if (i === 5) {
        break;
    }
    console.log(arr[i]);
}
```
Il est possible de stopper un loop en cours avec un break comme montré au-dessus, le loop s'arrêtera quand i sera égal 
à 5.

### _Le do... while... loop_

Cette boucle est particulière car elle sera exécutée au moins une fois, meme si les conditions données ne sont jamais 
vraies. Elle se présente de la manière suivante : 

```javascript
let i = 0;
do {
    console.log(i);
    i++;
}
while (i<10);
```

### _Le while loop_

Cette boucle est similaire à la do...while... sans le do, c'est-à-dire qu'elle ne sera pas forcément démarrée si la 
condition de départ donnée est fausse. Elle continuera tant que la condition "stop" ne sera pas atteinte. 

```javascript
let j = 10;
while (j > 0) {
    console.log(j);
    j--;
}
```
