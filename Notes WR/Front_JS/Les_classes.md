 # ___Les classes___
 
Une classe est un type de fonction qui permet de déclarer des objets. Elles sont pratiques car elles 
permettent de créer plusieurs objets sur une classe définie, elles correspondent en qqe sorte à un 
modèle d'objet.

## _Déclarer une classe_

Une classe se définit avec le mot clé "class" suivie du nom de cette classe avec une Majuscule.
```javascript
class User {
    name;
    age;
    type = "human";
    presence = true;
};
```
On peut ensuite lui définir des propriétés comme pour un objet, on pourra ensuite créer des objets à
partir de cette classe qui prendront les propriétés de celle-ci. Ici, les objets user et user2 auront par défaut les 
mêmes propriétés, prises sur la classe User.

    let user = new User();
    let user2 = new User();
    user.name = "Janne";

## _Le constructor_

La fonction _constructor_ est utilisée lors de la création d'une classe. Elle sert à définir des propriétés que chaque 
objet devra posséder lors de sa création. 

```javascript
class User {
    name;
    age;
    presence;
    pays = "France";

    constructor(name, age, presence = true) { //On donne des arguments a la fonction constructor qui sont les 
        // propriétés obligatoires lors de la création d'un nouvel objet. Ici presence a une valeur définie par 
        // défaut comme vraie.
        this.age = age;
        this.name = name;
        this.presence = presence;
        // ces 3 lignes servent a définir les arguments age, name et presence comme propriétés de l'objet créé 
        // this fait tjr référence à l'objet courant, donc à l'objet créé.
    }
}
let user = new User("Arthur", 28);
let user2 = new User("Rudette", 26, presence = false);
// ainsi, nous avons créé deux users à partir de la classe User, ils auront donc comme propriété pays = "France" tous 
// les deux, mais auront chacun leur nom et leur age défini lors de l'instanciation de l'objet.
// Ici, user aura la presence par défaut = true et user2 aura défini sa présence comme false.
```

## _Les méthodes de classes_

Il est possible de créer des méthodes dans des classes, ce qui signifiera que les objets créés à partir de la classe 
auront accès à cette méthode aussi. 

```javascript
class User {
    // reprenons l'exemple d'au dessus...
    isPresent() {
        return this.presence;
    }
    say(str) {
        console.log(this.name + " : " + str);
    }
}
user.say("Bonjour " + user2.name);
console.log(user.isPresent());
```

## _Héritage entre les classes_

On peut faire "hériter" une classe d'une autre classe avec le mot clé __extends__, elle reprendra les caractéristiques 
de la classe parente, c'est-à-dire, ses propriétés et ses méthodes ainsi que son constructor. Regarde mon petit
Arthurus, dans l'exemple qui suit : 

```javascript
class Champion {
    constructor(name, hp, baseAtt) {
        this.name = name;
        this.hp = hp;
        this.baseAtt = baseAtt;
    }
    attack(other, att) {
        return other.hp -= att;
    }
    moveRight(num) {
        console.log(this.name + "has moved on right" + num + " times");
    }
    moveLeft(num) {
        console.log(this.name + "has moved on left" + num + " times");
    }
}

class Cac extends Champion {
    constructor(name, hp, baseAtt, range = 50) {
        super(name, hp, baseAtt);
        this.range = range;
    }
}

class Cast extends Champion {
    constructor(name, hp, baseAtt, range = 300) {
        super(name, hp, baseAtt);
        this.range = range;
    }
}

let teemo = new Cast("Teemo", 300, 60, 350);
let twitch = new Cast("Twitch", 280, 58, 400);
let trist = new Cast("Tristana", 320, 65, 350);
let rumble = new Cac("Rumble", 380, 50, 50);

console.log(teemo.hp, teemo.baseAtt);
teemo.attack(twitch, teemo.baseAtt);
console.log(twitch.hp);
```

## _Les classes et propriétés statiques (static)_

On peut utiliser le terme "__static__" lors de la création d'une classe, que ce soit pour une propriété ou méthode.
Cela fera en sorte que la propriété ou la méthode ne soit pas accessible dans les instanciations de la classe càd dans 
les objets créés à partir de cette classe. 

```javascript
class Vehicule {
    constructor(name, wheels) {
        this.name = name;
        this.wheels = wheels;
    }
    static GRAVITY = 10; 
    // GRAVITY est une constante uniquement définie pour la classe Vehicule, on écrit les propriétés static et 
    // constantes en MAJ pour la reconnaitre facilement
    
    static testTechnique(vehicule) {
        return vehicule.wheels > 0;
    }
    // La méthode testTechnique() est une méthode "static" uniquement utilisable avec la méthode
}

let car = new Vehicule("Bugatti", 4);
console.log(car.testTechnique); // Ceci ne fonctionnera pas car on ne peux appeler une méthode static dans un objet instancié
console.log(Vehicule.testTechnique(car), Car.GRAVITY);
// Ce console.log() fonctionnera car on appelle la méthode via la classe et on met un objet en argument, la propriété 
// est appelée via la classe dans laquelle elle a été créée, pas par un objet.
```