# ___MANIPULER LE DOM___

## _Relier un fichier .js au html_

On lie un fichier JS à un fichier html en ajoutant un 
    
    <script src = nomdufichier.js> </script>

juste avant la fermeture du tag body. 

## _S'assurer que le dom existe pour pouvoir le modifier_

Il existe deux manières de s'assurer que le dom existe afin de le manipuler. On écrit le code suivant 
dans notre fichier .js

    window.onload = function () {
        // On met tout ce qui est manipulation de dom dans cette fonction
    }

    window.addEventListener('DOMContentLoaded', function() {
        // On met tout ce qui est manipulation de dom dans cette fonction
    })

Ceci permet d'écrire dans le document une fois que le document html est chargé dans son intégralité, 
ainsi les modifications seront faites sur qqchose dont on est sûr qui existe.

## _Sélectionner un élément via son ID_

Un ID est unique à un élément dans le code html, ils servent principalement pour manipuler les éléments en JS. 
Gardons en tête que ce qui suit est tjr compris dans l'une des deux fonctions données ci-dessus. 
Pour sélectionner un élément html avec un id, on devra faire : 

    ...
    document.getElementById("id de l'élément qu'on veut sélectionner");
    }

Le "document" qui est en premier dans notre ligne de code est l'ensemble du document html dans lequel tout le contenu 
est compris. En utilisant getElementById, on sélectionne l'élément qui est défini avec l'id donné, on peut ensuite le
modifier via le JS.

## _Sélectionner un élément par sa ou ses classes_

Pareil qu'une sélection par ID à la différence qu'une classe sera souvent utilisée pour plusieurs éléments au lieu d'un
pour les id. 

    ...
    document.getElementsByClassName("nom de la classe qu'on veut sélect");
    }

La conséquence de cette sélection multiple est que l'on va récupérer via cette méthode un tableau de plusieurs éléments,
on pourra cependant itérer dans notre tableau pour en sélectionner un en particulier. 

## _Sélectionner un ou plusieurs éléments_

Indépendamment de la classe ou de l'id, il est possible de sélectionner un élément seul ou en nombre. On utilise pour 
cela le querySelector / querySelectorAll : 

    ...
    const square = document.querySelector('sqr', '.square', '#sqr');
    // le querySelector va trouver et renvoyer le premier élément qui correspond a sa recherche
    // en mettant 'sqr', l'élément recherché pourra etre une classe ou un id
    // en mettant '.square', l'élément recherché sera une classe
    // en mettant '#sqr', on recherchera un id uniquement

    const squares = document.querySelectorAll('div.square.bigSquare');
    // On a changé querySelector en querySelectorAll => on va trouver ici tous les éléments qui correspondent à la recherche
    // ici, on a mis 'div.square.bigSquare', il va donc chercher des éléments se trouvant dans une div avec la classe 
    // square ainsi que la classe bigSquare !
    // le querySelectorAll renvoie un tableau de tous les éléments trouvés

## _Manipuler le style d'un élément_

Il est possible de manipuler le style d'un ou plusieurs éléments via le JS une fois l'élément sélectionné. On va dire 
qu'on a sélectionné un élément #sqr, un carré qu'on a stocké dans une const square : 

    ...
    square.style.backgroundColor = 'yellow';
    square.style.top = '0px';
    let positionY = 0, positionX = 0;
    setInterval (function() {
        positionY++;
        square.style.top = positionY + "px";
    }, 100)
    setInterval (function() {
        positionX++;
        square.style.left = positionX + "px";
    }, 100)

Dans l'exemple ci-dessus, on a changé la couleur du background de l'élément square, élément défini par la sélection 
précédent "const square = document.querySelector('#sqr');"
On a ensuite défini sa position puis créé deux fonctions pour qu'il se déplace d'un px sur la droite et vers le bas 
toutes les 100ms. 

## _Manipuler les classes d'un élément_

Il est aussi possible d'ajouter ou retirer des classes à un élément html via JS. Pour cela, il faut accéder à l'élément 
puis lui ajouter une méthode add ou remove de class comme ceci : 

```javascript
...
const square = document.querySelector("square");
square.classList.add('bigSquare'); // avec le .classList.add(), on ajoute la classe bigSquare à notre élément square
square.classList.remove('square'); // ici, on delete la classe square de l'élément, il n'aura que la classe .bigSquare
// On peut ajouter ou supprimer plusieurs classes en meme temps
```

## _Créer et ajouter des éléments au dom_

On peut, via JS ajouter des éléments à notre dom html. 

```javascript
...
const square = document.createElement("section"); // On créé ici un élément de type section vide
square.classList.add("square"); // Comme précédemment, on ajoute la classe square a notre élément
square.innerText = "Hello"; // On ajoute du texte avec .innerText
square.innerHTML = "<strong>Hello</strong>"; // On peut ajouter des balises html avec .innerHTML
document.body.appendChild(square); // Via .appendChild, on ajoute un élément enfant à (ici) body qui est notre square

let container = document.createElement("div"); // On créé un élément div vide
container.innerText("container"); // on lui ajoute le texte "container"
document.body.appendChild(container); // on l'ajoute au document en enfant de body
container.appendChild(square); // on ajoute un square dans container
```

## _Écouter les évènements du dom_

Il est possible "d'écouter" les évènements du dom et ajouter/modifier diverses choses lorsque cet évènement se produit.
C'est ce que l'on fait d'ailleurs quand on écrit notre code dans un window.addEvenListener('DOMContentLoaded', function()) ...
En effet, l'évènement est à ce moment "DOMContentLoaded", mais il existe de nombreux évènements web. On peut retrouver 
une liste exhaustive de ceux-ci sur [Mozilla](https://developer.mozilla.org/fr/docs/Web/Events).