# _Les fonctions_

Une [fonction](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Fonctions) est un type d'objet particulier qui permet de définir une manière de faire des choses de manière ordonnée.
On définit une fonction avec le mot function, une fonction prend en charge ce qu'on appelle des __arguments__. Les arguments
sont des données qui sont modifiables pendant l'appel d'une fonction.

## _Les fonctions anonymes_

Une fonction est dite __anonyme__ quand on ne lui donne pas de nom, elle sera souvent stockée dans une variable et sera
appelée grace à cette dernière.

```javascript
let fn = function () {
    return 0;
}
```

## _Les fonctions callback_

On appelle une fonction "callback" quand cette dernière est utilisée en argument d'une autre fonction. Cela revient à
imbriquer une fonction avec des process dans une autre fonction qui fait des choses différentes.

```javascript
function myCallback(num) {
    return num * 3;
}

function myFunction(num, callback) {
    let nbr = num * 10;
    return callback(nbr);
}

function myCallback2 (num) {
    return myCallback(num * 5);
}
console.log(myFunction(5, myCallback), myFunction(5, myCallback2));
```

Attention, quand une fonction est appelée en argument d'une autre fonction, il ne faut pas lui mettre de parentheses ni
d'arguments, c'est ce qu'on appelle la "__référence__" de cette fonction.

## _L'objet arguments_

L'objet __arguments__ est un tableau rassemblant les arguments d'une fonction. On a donc un array composé des arguments
d'une fonction, son index de départ est 0 et il possède une length, cependant on ne peut pas utiliser avec lui les
méthodes des arrays.

```javascript
function myFunction() {
    console.log(arguments);
}
myFunction(1, 2, 3, 4);
// Cette fonction écrira donc dans la console les 4 arguments entrés lors de l'appel de la fonction
function myFn() {
    if (arguments[5] === "Sam") {
        alert("Vous etes Sam");
    }
}
myFn(1, 3, 5, 7, 9, "David");
myFn(1, 3, 5, 7, 9, "Sam");
// Cette fonction envoie une alerte si le 6e argument est "Sam", le premier call n'enverra pas d'alerte mais le 2nd oui.
```

## _La fonction arrow_

La fonction arrow est différente de la fonction classique de par sa notation, elle a aussi un fonctionnement différent, 
notamment avec le __"this"__.  
En effet, dans une fonction classique, le this sera propre a la fonction. 
Avec la fonction fléchée, le this sera lié à celui du parent, c'est-à-dire que si l'on crée une fonction fléchée dans 
une classe, le this de la fonction fera référence à l'objet qui sera créé à partir de cette dernière.

La méthode [__".bind()"__](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Function/bind) 
peut aussi être utilisée pour lier un objet à une fonction. Elle crée une nouvelle fonction qui, lorsqu'elle est appelée, 
aura comme "this" l'argument avec lequel elle a été appelée.

```javascript
const module = {
  x: 42,
  getX: function() {
    return this.x;
  }
};

const unboundGetX = module.getX;
console.log(unboundGetX()); // The function gets invoked at the global scope, return this.x est le this de window
// expected output: undefined

const boundGetX = unboundGetX.bind(module); // revient a faire unboundGetX = module.getX.bind(module)
// On lie unboudGetX au this de module, donc return this.x va retourner module.x et non plus window.x
console.log(boundGetX());
// expected output: 42
```
