# _Arrays_

Les __arrays__ ou __tableaux__ servent à stocker et manipuler des données de différents types (str, num ...) de manière
ordonnée. Ils peuvent être de type constante ou variable et il est _mieux de les remplir avec des éléments de meme type_
. Les tableaux commencent à l'index __0__ ! Il est possible d'avoir des tableaux imbriqués les uns dans les autres.

## _Création d'un array_

Les tableaux peuvent êtres créés de différentes manières :

```javascript
const arr = ["Arthur", null, 1, true [1, "2", []]];

const arr2 = new Array(5);
// il n'y a qu'un chiffre, le 5 définira donc la longueur de l'array
const arr3 = new Array(5, 6, 7, 8);
// si il y a plusieurs chiffres, ils seront des éléments de l'array
const arr4 = new Array(10).fill(null);
// si on veut remplir un array avec une même valeur, on peut utiliser la méthode .fill() pour gagner du temps
```


## _Méthodes essentielles pour les tableaux_

Ces méthodes possèdent toutes ou pour la plupart une fonction callback qui permet de définir ce que nous voulons
tirer de l'utilisation de la méthode.  
Il existe de nombreuses méthodes qui permettent de manipuler les tableaux plus facilement, nous allons en voir
quelques-unes.  
Nous retrouvons une liste de chacune de ces méthodes sur la page de [mozilla](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array).

```javascript
const arr = [];
arr.push("hello", "bonjour", "salam");
// le .push ajoute de nouvelles valeurs a la fin de l'array
arr.splice(1, 1, "aligato"); 
/* le .splice() accepte différents arguments, le premier est le 'start', quelle va etre 
la valeur sélectionnée, le 2e est le nombre d\'éléments à supprimer ou remplacer, le 3e est l\'élément par lequel on va 
remplacer la précédente sélection */
```

### _Méthode .forEach()_

La méthode .forEach() va passer par chaque valeur d'une array, son but est de permettre d'utiliser une fonction pour
chacun de ces éléments. On définit par défaut les arguments de la fonction comme la value, l'index et l'array en elle
meme.

```javascript
arr = [1, 2, 3, 4]
arr.forEach(function (value, index, array) {
    console.log(index, value * 2);
});
//ici on va afficher dans la console l'index de chaque valeur de l'array et sa valeur multipliée par 2
```

### _Méthode .find()_

La méthode .find() sert à chercher un élément parmi tous les éléments d'un array, elle renvoie l'élément trouvé.

```javascript
let arr = ["Saloute", "Bijour", "Camasa", "Si bueno"];

let result = arr.find(function (element) {
    return element.includes('bu');
});
console.log("result :", result);
// On a donc trouvé grace a .find() l'élément 'bu' dans l'array arr et on aura dans la console result : Si bueno.
```

### _Méthode .findIndex()_

Cette méthode permet de chercher un élément comme .find() et de renvoyer l'index de cet élément d'un array.

```javascript
let arr = ["Saloute", "Bijour", "Camasa", "Si bueno"];

let result = arr.findIndex(function (element) {
    return element.length > 6;
});
console.log("result :", result, arr[result]);
// On a trouvé le premier élément de plus de 6 charactères qui est "Saloute", la methode findIndex() renvoie donc dans 
// notre variable result l'index de l'element : ici 0
// Cette méthode et la méthode find ne peuvent que trouver le premier "match" de la condition
```

### _Méthode .map()_

Cette méthode permet de créer et définir les valeurs d'un tableau à partir d'un tableau déjà existant. La valeur de
retour de la fonction callback dépend de ce qu'on veut créer.

```javascript
let arr = [1, 2, 4, 8, 16, 32, 64, 128];

let arr2 = arr.map(function (element) {
   return {age: element}
});
console.log("arr2 =", arr2);
// Ici, grâce à .map(), on a pu créer un tableau d'objets ayant pour value : element et pour key : age
// {age: 1}, {age: 2}, {age: 4}, {age: 8}, ...
```

### _Méthode .reduce()_

Cette méthode permet de réduire un tableau rempli de nombres à une valeur unique qui sera souvent la somme de toutes les
valeurs de l'array. La fonction .reduce() à __deux__ arguments, une fonction callback et un accumulateur (qui est défini
au départ et souvent set à 0).

```javascript
let arr = [1, 12, 18, 24, 132, 264];

let accumulation = arr.reduce(function (accumulateur, value, index) {
    accumulateur += value;
    return accumulateur;
}, 0 /* 0 est le start de l'accumulateur */ );
console.log(accumulation);
// accumulation est donc ici égal à la somme de toutes les valeurs de arr
```

### _Méthode .filter()_

Cette méthode va retourner un tableau en filtrant, selon les conditions que l'on définit, les valeurs d'un array.
Si le retour est "true", la valeur reste dans le nouveau tableau, si le retour est "false", la valeur est filtrée du
tab.

```javascript
let arr = [1, 12, 18, 24, 132, 264];

let arrFiltred = arr.filter(function (element, index) {
    return (element > 20 && element < 150);
});
console.log("Notre array filtrée est :", arrFiltred);
```

### _Méthode .some()_

Cette méthode va itérer sur chaque valeur de l'array pour chercher une correspondance. Elle renverra true si la condition
a été trouvée au moins une fois parmi les valeurs et false si aucune des valeurs de l'array n'a vérifié la condition.
Elle s'arrête d'itérer quand une correspondance avec la condition a été trouvée.

```javascript
let arr = [1, 12, 18, 24, 132, 264];

let condi = arr.some (function (element, index) {
    return element > 1000;
});
console.log("Ma condition est elle vraie pour au moins une valeur ?", condi);
// Ici, false sera renvoyé car aucune valeur de mon array n'est plus grande que 1000
```

### _Méthode .every()_

Cette méthode itère sur chaque valeur d'un array et vérifie qu'elles valident __TOUTES__ une condition donnée.

```javascript
let arr = [1, 12, 18, 24, 132, 264];

let condi = arr.every (function (element, index) {
   return element < 1000; 
});
console.log("Est ce que toutes mes valeurs vérifient la condition ?", condi);
//Ici, on aura true car toutes les valeurs de arr sont < 1000
```

## _Méthodes JS sympas et utiles_

### _Le spread operator (...)_

Le spread operator (qui s'écrit "...") permet d'étaler ou de copier des données de différentes structures comme des
arrays ou des objets.
Il est très pratique et utilisé, notamment pour créer de nouveaux objets qui prendront des propriétés d'un objet type
du même genre, permettant après de redéfinir les données à changer en gardant les données de bases similaires aux
différents objets.  
__//!\\ En JS, on doit passer par cette méthode, car si on essaye de "copier" un objet ou autre type de donnée en faisant :  
let orange = fruit ==> cela copierait la référence et tout changement opèrerait sur les deux objets ce qui n'est pas le
but ! //!\\__
```javascript
let arr1 = [1, 2, 3], arr2 = [2, 4, 6];
let arr3 = [...arr1, ...arr2];
```

### _Affectation par décomposition_

L'affectation par décomposition permet de décomposer un tableau ou un autre type de donnée pour affecter ses composantes
à d'autres données, par ex des variables.

```javascript
let [a, b, c, ...rest] = [1, 2, 3, 4, 5, "et plus encore"];
// ici, on aura donc défini 4 variables a, b, c et rest qui prendront les valeurs du tableau suivant 
// a = 1, b = 2, c = 3, rest = [4, 5, "et plus encore"]
// on fait ici une affectation par décomposition pendant une déclaration car on affecte aux variables en meme temps 
// qu'on les définit.
```