# ___REACT___

## _Intro_

[React](https://fr.reactjs.org/docs/getting-started.html) est une librairie JS, elle n'est pas considérée comme un 
framework à l'inverse d'Angular, elle est de taille faible et est très adaptable à tous les besoins. Le fait est qu'elle 
est utilisée en parallèle d'autres librairies qui permettent de s'adapter aux besoins, en découle cependant une certaine
complexité du fait de champs des possibles.  
On notera aussi qu'il est possible grâce à React de coder du web autant que de l'IOS ou de l'android 
ce qui en fait une techno recherchée par les entreprises. 

## _Utiliser React dans un projet_ 

React étant une librairie, il suffit d'ajouter les liens CDN dans le html pour pouvoir l'utiliser.  
Il y a deux liens pour le développement et deux liens minimisés en taille pour la production. L'un des liens est l'élément
React et le deuxième est l'élément react-DOM.  
https://fr.reactjs.org/docs/cdn-links.html  
Il suffit ensuite de linker son fichier JS dans le html dans un \<script src="mon-fichier.js">\</script>

## _Créer une app React_

https://create-react-app.dev/docs/getting-started#yarn

    npx create-react-app mon-app
    cd mon-app
    npm start

Pour la version finale, dite "production", on lance un "npm run build" et tout passe en version mimifiée

## _Le React et le JSX_

Le JSX est un type de code qui permet de faciliter l'écriture de code pour React. Il est optionnel cependant et on peut 
choisir de coder en react sans l'utiliser. Il est nécessaire pour que le code soit lisible par les navigateurs d'utiliser 
un compileur (ici babel). 
Le fichier .js sera un .jsx, et il faudra rajouter, dans le html, avant l'ajout du fichier .jsx l'ajout du compileur.  
On aura donc : 

    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script> 
    <script src="mon-fichier.jsx" type="text/babel"></script>

Il est aussi possible de passer par Node pour installer JSX et babel dans notre projet, il suffit de taper ces lignes
dans la console : 

    npm init -y
    npm install babel-cli@6 babel-preset-react-app@3

https://fr.reactjs.org/docs/add-react-to-a-website.html#quickly-try-jsx

## _Le typage en JSX_

![image1](./assets/img/Capture%20d’écran%20(66).png)

On voit que le typage en JSX est un mélange de JS et de html 
Dans l'image ci-dessus, on voit que l'on crée une fonction render(), dans celle ci on a un array tags  
On crée une const tagsEl et on la définit comme un array rempli par les composants de "tags" mais entourés de 
\<li>\</li> avec une key définie comme l'index

Les <React.Fragment> servent a avoir des "div" invisibles, React ne prend en charge qu'un élément html par élément, 
donc dans notre const title, on doit avoir un élément qui encadre tous les autres pour que ce soit pris en charge par 
react.

On voit que la classe dans react est appelée className="title", si on veut la rendre dynamique avec par ex l'implémentation 
du n (implémentée au dessous, on ne le voit pas sur l'image), on aurait dû faire comme pour l'id => {"title"+n}

La dernière ligne ReactDOM.render(...) sert à placer l'élément dans le html, ici on le place dans l'élément avec l'id "app"

## _Cycle de vie d'un composant_

Chaque composant React passe par différentes étapes durant lesquelles il est possible d'intervenir. On appelle ça le
cycle de vie d'un composant. Celui-ci se découpe en 3 parties :

- Mount : le montage. Il intervient quand une instance du composant est créé dans le DOM.

    - constructor()
    - render()
    - componentDidMount()

- Update : la mise à jour. Ce cycle de vie est déclenché par un changement d'état du composant.

    - shouldComponentUpdate(nextProps, nextState, nextContext)
    - render()
    - componentDidUpdate()

- Unmount : le démontage. Cette méthode est appelée une fois qu'un composant est retiré du DOM.

    - componentWillUnmount()



Tout s'articule autour de ces 3 cycles de vies.

https://fr.reactjs.org/docs/react-component.html#the-component-lifecycle

## _Le "state" des composants_

setState() planifie la mise à jour de l’objet state du composant. Quand l’état local change, le composant répond en se
rafraîchissant.  

Actuellement, setState est asynchrone à l’intérieur des gestionnaires d’événements.  

Ça permet de garantir, par exemple, que si Parent et Child appellent tous les deux setState lors d’un clic, Child ne
sera pas rafraîchi deux fois. Au lieu de ça, React « apure » les mises à jour de l’état à la fin de l’événement du
navigateur. Ça permet une amélioration significative des performances pour les applications de grande ampleur.

```
function Count() {
    const [count, setCount] = useState(0);
    return (
        <div>
            <p>Vous avez cliqué {count} fois</p>
            <button onClick={() => setCount(count + 1)}>
                Cliquez ici
            </button>
        </div>
    );
}
```

https://www.youtube.com/watch?v=-PVCxQM-bBI&feature=youtu.be

## _L'affichage conditionnel_

En React JSX, il y a une manière de vérifier des conditions en plus des if statements : 

    notre expression à tester && ce qu'on veut afficher 
    this.state.nb >= 1 && <button onClick={( => this.decrement()}> Décrémenter </button>

On voit ici qu'on a notre expression conditionnelle : this.state.nb >= 1, si elle est fausse, react va sauter le reste, 
si elle est vraie, react va afficher la suite. 
Il est aussi possible d'utiliser des if statements ou du ternaire.

## _Les listes keys_

Les keys sont des éléments utilisés par React pour noter quels éléments modifier ou supprimer dans une liste, les keys 
sont souvent définies grace aux id et aux index. 

On peut construire des collections d’éléments et les inclure dans du JSX en utilisant les accolades {}.

Ci-dessous, on itère sur le tableau de nombres en utilisant la méthode JavaScript map(). On retourne un élément li
pour chaque entrée du tableau. Enfin, on affecte le tableau d’éléments résultant à listItems :

```javascript
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
    <li>{number}</li>
);
```

On inclut tout le tableau listItems dans un élément ul, et on l’affiche dans le DOM :

```javascript
ReactDOM.render(
    <ul>{listItems}</ul>,
    document.getElementById('root')
);
```

__Les clés__ aident React à identifier quels éléments d’une liste ont changé, ont été ajoutés ou supprimés. Vous devez
donner une clé à chaque élément dans un tableau afin d’apporter aux éléments une identité stable :

```javascript
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
    <li key={number.toString()}>
        {number}
    </li>
);
```

Le meilleur moyen de choisir une clé est d’utiliser quelque chose qui identifie de façon unique un élément d’une liste
parmi ses voisins. Le plus souvent on utilise l’ID de notre donnée comme clé.

## _Faire remonter une donnée d'un enfant à un parent_

https://youtu.be/r8CCKLIFPKk

## _Les formulaires React_ 

https://www.youtube.com/watch?v=DYISuMHXz6M&feature=youtu.be

## _Le routing React_

https://reactrouter.com/web/guides/quick-start

![image1](./assets/img/Capture%20d’écran%20(69).png)
![image1](./assets/img/Capture%20d’écran%20(70).png)
