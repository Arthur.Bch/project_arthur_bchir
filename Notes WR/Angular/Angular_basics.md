# ___Angular___

## _Introduction_
Angular est une single-page-application, cela signifie que c'est le navigateur qui prend en charge le code, tout est 
chargé dans une seule page. Angular utilise TypeScript, notre application communique avec un serveur pour afficher les 
pages. 

[Angular](https://angular.io/docs) est utile pour ce qui est du domaine des applications web, de par sa forme, dans le domaine du city-commerce et 
les dash-boards (récupérer des données sur différentes sources et les afficher).
Angular est peu adapté pour le développement de sites web basés sur du contenu (SPA => only JS => moteurs de recherche 
ne sont pas forcément capables d'exécuter le script => pas de référencement). Il est aussi peu adapté pour des petits 
changements sur du petit contenu car c'est un framework CONSEQUENT, lourd.

## _Commandes_

[Doc officielle](https://angular.io/cli)

npm install -g @angular/cli => installer Angular  
ng new my-project => créer un nouveau projet Angular nommé my-project  
ng => voir les différentes commandes a placer apres ng  
ng analytics project off => stopper le partage de données anonymes sur un projet

## _Structure d'un projet Angular_

### _1. Dossier e2e_

Cela fait des tests dit "n2n", ce sont des tests coté navigateur

### _2. Dossier node_modules_

Contient les dépendances du projet

### _3. Dossier src_

Contient tout notre projet

#### _3.1. Dossier app_
C'est ici qu'on va faire l'essentiel des modifications !
Ici, on va le séparer entre les components et les modules en créant un nouveau dossier components et un dossier app 
dans celui-ci.  
Il est possible de créer de nouveaux composants via le terminal avec le commande __"ng g(ou generate) c(ou component) nomDuCompo"__
Ex : "ng g c home"

On peut ensuite se servir d'un composant en les appelant dans le .html d'un autre composant avec leur selector  
Par ex, je vais ajouter dans mon __app.component.html__ la balise __<app-home></app-home>__ pour faire apparaitre le html de mon 
fichier __home.component.html__

### _4. Le fichier angular.json_

On peut définir des informations sur notre projet, la configuration

### _5. Le fichier package.json_


## _Interpolation et Propriety binding_

On voit sur les images suivantes l'utilisation de l'interpolation avec les {{ title }} et du propriety binding avec les "innerHTML" et "innerText".
![image1](./assets/img/Capture%20d’écran%20(31).png)
![image2](./assets/img/Capture%20d’écran%20(28).png)
![image3](./assets/img/Capture%20d’écran%20(29).png)

## _Events binding_ 

C'est la liaison des events dans angular, autrement dit, la liaison entre un élément angular et un évènement (ex: click).
On va faire le binding dans le component.html et on va définir, comme pour les propriety, les changements à opérer dans 
le component.ts (dans la classe du composant)  
__Pour les évènements, on utilise des () et pas des [] (qui sont pour les propriétés) !!__

![image1](./assets/img/Capture%20d’écran%20(34).png)
![image1](./assets/img/Capture%20d’écran%20(35).png)
![image1](./assets/img/Capture%20d’écran%20(36).png)

Le #inputRef dans le fichier app.component.html est ce qu'on appelle une référence, cela crée une variable nommée (ici inputRef)
qui sera une référence à l'input html.
On voit qu'on a créé une fonction qui, à chaque fois que l'event (change) est écouté, donc qu'il y a un changement sur 
l'input, la var text va changer et prendre la valeur de l'input.
On peut utiliser une notation qui va permettre de récupérer les données d'un évènement, il s'agit de __"$event"__ que l'on 
place dans le code html après l'event. 

## _Lier des données dans les deux sens ou 2 ways data binding_

Il est possible de faire une liaison dans les deux sens, c'est-à-dire qu'on lie une variable à une propriété html dans 
les deux sens. Sur l'exemple suivant, on voit que le text est défini dans app.component.ts par défaut, il prendra donc 
cette valeur de base dans le html. Ensuite, s'il est modifié via le navigateur avec "input", il changera la valeur de 
la variable "text".  
On fait ceci grace au mot clé __"[(ngModel)]"__ qui est un composant Angular. On doit cependant l'ajouter à notre projet 
en important dans app.module.ts "FormsModule" qui permettra l'utilisation de [(ngModel)]. Il est adapté à l'élément 
html input de par sa nature __2 ways data binding__.

![image1](./assets/img/Capture%20d’écran%20(42).png)
![image1](./assets/img/Capture%20d’écran%20(43).png)
![image1](./assets/img/Capture%20d’écran%20(44)_LI.jpg)

## _Structural directif *ngIf_

Le "___*ngIf___" est une sorte de checkup de conditions pour voir si une variable répond bien à des conditions avant d'être, par
exemple, affichée dans du HTML. 
On s'en sert à peu près de la meme manière qu'un if en JS natif. 
```html
<h1> { titre } </h1>

<p *ngIf="user; else userIsNull">{{ user.name }}</p> <!-- Avec le *ngIf, on vérifie que l'objet user contenu dans le 
fichier "app.component.ts existe bien et n'est pas null et de l'afficher si c'est le cas, le else nous dit d'afficher 
la ref userIsNull si il est null, elle est définie avec le #userIsNull au dessous--> 

<ng-template #userIsNull>
    <p>User is null</p>
</ng-template>
```

## _Structural directif *ngFor_

Le "___*ngFor___" est une manière de faire un loop dans du html sur Angular. 

```html
<h1> {{ titre }} </h1>

<p *ngFor="let user of users; let id = index"> <!--On définit ici un loop sur le tableau users (défini dans le fichier 
"app.component.ts") la variable user va looper sur chaque objet du tableau, id va récupérer les index de chaque user-->
    <div> {{user.name}} - {{ id }} <!--On affiche le name de chaque user et id (donc l'index)-->
        <button (click)="changeUserName(user), supprUser(id)">Change user.Name</button>
    </div> 
</p>
```
```typescript
// ceci est dans le fichier app.component.ts, d'où l'écriture sans définition ect ...
users: {name: string}[] = [
    {name = "Jean"}, 
    {name = "Arthur"}
    ];

changeUserName(user) {
    const users2 = {...user};   // ici, on définit un user2 qui reprend les props de user pour ne pas modifier user en 
                                // utilisant la fonction, on veut en règle générale ne pas travailler sur les références
    user2.name = "new Name";    // des éléments
    console.log(user2, user);
}

supprUser(id) {
    let users2 = [...this.users];   // On définit ici un nouveau tableau qui reprend le tableau users (on peut le modifier
                                    // sans modifier le tab de référence users)
    users2.splice(id, 1);           // on enleve l'objet quand on clique sur le bouton associé du nouveau tableau
    this.users = users2;            // On redéfinit notre tableau users a partir de users2, on supprime donc le user 
                                    // de users aussi.
}
```

## _Utiliser *ngIf et *ngFor sur les mêmes éléments_

Dans le cas où on a besoin d'utiliser un if et un for sur les mêmes éléments, c'est possible, il suffit de créer un 
élément "<ng-container></ng-container>" dans lequel on mettra notre *ngFor. A l'intérieur, on pourra mettre notre 
"<div></div>" contenant notre *ngIf comme on peut le voir sur cette [video](https://youtu.be/E2KwZMS6KyI?t=1096).


Regarde un résumé d'un peu tout ce qui précède dans cette [VIDEO](https://www.youtube.com/watch?v=26sDYakDeg0&feature=youtu.be)

## _Découper les éléments en différents composants_

Il est possible de découper une page par composants, chacun correspondant à un comportement.
Pour créer un nouveau composant, on peut le faire à la main ou passer la console en utilisant la commande : "ng generate
component nom_du_composant"  
Cela va créer un nouveau composant dans "./my-app/src/app" que l'on rangera dans un dossier à son nom qui sera lui-meme
dans le dossier "components" que l'on a créé pour plus d'organisation. 
On s'assurera après déplacement que le chemin d'accès indiqué dans le fichier "app.module.ts" est tjr le bon

Il est possible de hiérarchiser les composants en enfants / parents

## _Partager des données à un enfant : @Input() decorator_

Pour partager des données d'un composant parent à un composant enfant, on utilise le @Input('eventuel_rename') + decorator 
que l'on met dans le composant enfant. 
Il faut aussi linker la variable dans le template du parents !
![image1](./assets/img/Capture%20d’écran%20(49)_LI.jpg)

## _Emettre des données au parent : @Output() decorator : new EventEmitter_

Pour récupérer des données d'un enfant depuis un parent, on se sert du @Output() qui se trouvera aussi dans 
le composant enfant.
![image1](./assets/img/Capture%20d’écran%20(52).png)
![image1](./assets/img/Capture%20d’écran%20(53).png)

