# ___ANGULAR V2___

## _Les modules_

Les modules sont composés d'un ou plusieurs composants, ils ne contrôlent pas d'HTML dans une application mais permettent
de définir quels composants peuvent être utilisés par quels composants, parfois dans d'autres modules. Ils sont présents
pour gérer les composants, qui eux, gerent le template HTML, le code TS, les pipes, et les services.  
On peut voir les modules comme de grands conteneurs dans lesquels sont les composants

Vidéo : https://www.youtube.com/watch?v=sK_IDLCwz_k&feature=youtu.be

## _Le routing_

https://www.youtube.com/watch?v=4G_Uzu1KVGA&feature=youtu.be  
https://www.youtube.com/watch?v=CX3lbqsAtzA&feature=youtu.be  
https://www.youtube.com/watch?v=3Hna49OPdEU&feature=youtu.be

## _Programmation dynamique with RxJs_

[RxJs](https://rxjs.dev/guide/overview) est une librairie qui possède des fonctionnalités basées sur les events, 
pratique pour coder dynamiquement. Il est possible de l'installer via npm puis de l'importer dans un fichier ou bien 
d'ajouter dans des balises (CDN) \<script> https://unpkg.com/rxjs/bundles/rxjs.umd.min.js \</script>

![image1](./assets/img/Capture%20d’écran%20(65).png)

Comme on le voit sur l'image, on définit un Observable qui est une classe intégrée à RxJs, qui sera un flux que l'on 
veut observer.  
Il faut toujours souscrire via la fonction .subscribe() à ce que l'on veut observer, ici définit comme la const flux.  
La fonction .next() permet d'ajouter des values.  
.error permet d'afficher des erreurs et .complete signifie que l'observation est terminée.

- Les flux sont écrits avec un $ à la fin de leur nom : const flux$
- On utilise Subject à la place de Observable si on veut l'observer plusieurs fois
- Behavior peut etre rajouté avant Sub/Obs si on à une valeur par défaut de notre observable

Il est très important, quand on a fini d'observer un élément d'unsubscribe via .unsubscribe() ! 

## _Les services_

Un service est un moyen de rendre une classe accessible à d'autres endroits que sa place premiere, notamment dans
d'autres composants. Cette classe sera prise en charge par Angular et le but est d'accéder, de n'importe ou à la meme
instance de cette classe.  
Pour cela, on doit ajouter lors de la création de notre service la fonctionnalité Angular @Injectable.

    import { injectable } from '@angular/core';
    @Injectable({
      providedIn: 'root' // ici on l'injecte directement dans la racine, on y aura accès partout
    })
    export class UsersService {
    ...
    }

Il est aussi nécessaire d'ajouter la dépendance quand on crée un service, cela peut etre fait directement dans le ou les 
composant qui utilisent ce service ou bien dans le fichier app.module.ts si on l'utilise à bcp d'endroits. 
On le déclare dans @NgModule => providers: []
On doit aussi, quand on utilise l'instance de la classe, ici UsersService dans un autre fichier définir cette instanciation
dans le constructor de l'autre fichier : 

    ...
    constructor(private usersService: UsersService) {}
    ...

Video liée : https://www.youtube.com/watch?v=wFgjhxzdeZk&feature=youtu.be

## _Les forms Angular_

https://youtu.be/uTTxz-xCpew  
https://youtu.be/iMuQEK9TEhc
