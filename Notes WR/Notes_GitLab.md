# :monkey_face: ___NOTES DAY 1___ :monkey_face: 


## _Créer un repository sur gitlab et l'exporter sur WS_

 - new project -> faire un choix -> le nommer et définir sa visibilité -> inviter des personnes et définir leurs droits. 
 - clone with https -> dans WS : Git, clone, copier l'URL

## _Création de branches et travail d'équipe_

 - En créant un nouveau projet, on est propriétaire, seul capable de modifier la branche "master" de fichiers d'un projet
 - Pour modifier, on créer des branches (ou forks) qui permettent de modifier le fichier de son coté sans modifier le fichier initial (master souvent)
 - Puis, il faut passer par plusieurs étapes pour que le fichier soit pris en compte sur le MASTER
 - git-commit : préparer le fichier modifié + faire un message pour signaler quelles modifications on été faites dans ce commit
 - git-push : pusher le commit ce qui changera le repository git (sur la branche dans laquelle on travaille)
 - retourner sur gitlab, dans le projet et soumettre une "merge request" qui devra être approuvée pour que merge, il y ait lieu


### *Problèmes de versions lors de merges* 

 - Il peut y avoir des problèmes de compatibilité entre les branches, par ex. si la branche master récupérée n'est pas à jour par rapport à sa dernière version
 - Dans ce cas, reprendre la branche master, la pull pour l'update puis retenter une merge

## _Faire des notes en MarkDown_

<body>
* ou _ permet d'écrire le texte en italique. <br>
** ou __ permet d'écrire le texte en gras. <br>
\ permet de revenir a la ligne <br>
` permet d'écrire des lignes de code <br>

`du gros code sa madré`

` répété 3 fois + le nom du langage permet d'écrire des lignes de code dans un langage donné
 ``` javascript
 for (
        let i = 1 ; i < 21 ; i++
        ) 
        
 console.log(i)
 ```
 
</body>


## _Raccourcis WS utiles_

 - _git-commit_ : __Ctrl-K__    
 - _git-push_ : __Ctrl-Maj-K__  
 - _git-pull_  : __Ctrl-T__ 
 
