# ___NODE BASICS___

## _Les API_

Les API sont l'abréviation de Application Programming Interface, elles servent d'interface entre les ressources d'un 
serveur et l'interface utilisateur. 
![image1](./Assets/Img/Capture%20d’écran%20(71).png)

L'API est donc, pour une action client, une requete et une réponse.

## _HyperText Transfer Protocol_

![image1](./Assets/Img/Capture%20d’écran%20(72).png)

Lors de la réponse à une requête, on revoie souvent un code HTTP pour dire différentes choses sur l'action réalisée, ils 
sont retrouvables [ici](https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP).

## _Tester son API_

Il est important de tester ses routes lors du développement d'une API, on utilise pour cela un logiciel (Insomnia, Postman).


## _Créer une API_

Pour commencer, il faut définir une const avec une require pour dire que l'on va faire une API http 

    const http = require('http');

On crée ensuite un serveur qu'on écoutera 

    const server = http.createServer(function(request, response) {
    });
    server.listen(8080);

On a ici créé un serveur http que l'on écoutera sur le port 8080 du localhost.
La fonction avec deux arguments request, response sert à récupérer des informations et en ajouter lors de la création du
serveur.
Pour le lancer, on doit lancer dans la console le fichier contenant ce code avec "node ficher.js"

## _Mise en place de Express_

[Express](http://expressjs.com/) est le framework pour node.js le plus utilisé, il sert a gérer les routes de manière
plus simple que nodeJS natif.

### _Installation_

![image1](./Assets/Img/Capture%20d’écran%20(74).png)

Il est aussi recommandé de créer un fichier .gitignore et de mettre /node_modules et /.idea dedans

### _Créer un serveur_

![image1](./Assets/Img/Capture%20d’écran%20(75).png)

On voit qu'avec express, il suffit de require('express'); pour pouvoir l'utiliser  
Ensuite, on définit notre serveur avec const server = express();  
Puis il suffit de l'écouter avec server.listen(le-port-d'écoute);

### _Middleware_

Les fonctions de [middleware](http://expressjs.com/fr/guide/using-middleware.html) sont des fonctions qui peuvent accéder à l’objet Request (req), l’objet response (res) et à 
la fonction middleware suivant dans le cycle demande-réponse de l’application.

Dans l'exemple de la video, on fait 
    
    server.use(express.json());

Cela permet de pouvoir utiliser un request.body.user, c'est le .body qui est important ici, il permet d'accéder au 
contenu.

Une bonne explication du json pour express : https://stackoverflow.com/questions/23259168/what-are-express-json-and-express-urlencoded#answer-51844327

### _Parametres de route et contenu de la requête_

![image1](./Assets/Img/Capture%20d’écran%20(76).png)

On voit qu'en mettant "/:id" a la fin d'une route permet d'avoir un parametre dynamique qui permettra de l'utiliser 
ensuite avec un, par exemple, request.params.id => cela va renvoyer l'id passé dans la route, par exemple, si on tape 
dans l'URL http://localhost:8080/users/4, on aura en réponse pour notre put le message "User updated successfully!", un
user : 4 et data: le contenu du user. 

Test sur Insomnia
![image1](./Assets/Img/Capture%20d’écran%20(77).png)

### _Gérer les erreurs et changer les status d'une réponse_

Il est important de renvoyer des status ou des erreurs en fonction de ce que l'on reçoit dans une request ou ce que l'on
envoie dans une réponse. 

    response.status(201).send(...);

Ici on renvoie un code 201 lors de la réponse  
Il faut faire les vérifications avant de renvoyer une réponse dans une méthode
