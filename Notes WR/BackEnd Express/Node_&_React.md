# ___Node & React___

## Présentation

Axios est une bibliothèque qui nous aide à faire des requêtes http à des ressources externes. Dans nos applications
React, nous avons souvent besoin de récupérer des données d'API externes afin qu'elles puissent être affichées dans nos
pages Web. Axios utilise les méthodes http comme get () et post () qui exécutent des requêtes http GET et POST pour
récupérer ou créer des ressources.

Lien utile : https://medium.com/codingthesmartway-com-blog/getting-started-with-axios-166cb0035237

## Exemple d'utilisation d'axios

```javascript
import React from 'react';

import axios from 'axios';

export default class PersonList extends React.Component {
    state = {
        persons: []
    }

    componentDidMount() {
        axios.get(`https://jsonplaceholder.typicode.com/users`)
            .then(res => { // then est l'équivalent d'une 'promise' pour être asynchrone
                const persons = res.data;
                this.setState({persons});
            })
    }

    render() {
        return (
            <ul>
                {this.state.persons.map(person => <li>{person.name}</li>)}
            </ul>
        )
    }
}
```
