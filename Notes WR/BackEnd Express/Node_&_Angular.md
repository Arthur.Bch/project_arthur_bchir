# ___Node & Angular___

## _Effectuer des requêtes http_

### _Faire l'import_

En premier lieu, il faut importer dans les imports d'Angular, "HttpClientModule" dans app.modules.ts 

### _Récupérer des données sur un composant Angular_

![image1](./Assets/Img/Capture%20d’écran%20(78).png)

On voit ici qu'on commence par mettre notre http dans notre constructor, puis, à l'initialisation, 
on récupère les users qui sont sur le serveur

Voici notre server.js qui se trouve dans notre repo backend
![image1](./Assets/Img/Capture%20d’écran%20(79).png)

### _Résoudre les conflits de ports d'écoute_

Quand on lance notre serveur Angular, il est souvent sur le port localhost:4200, or, nous avons utilisé le port
localhost: 8080 sur express pour notre server backend. 
Pour résoudre ce probleme, on utilise CORS (Cross-Origin Ressource Sharing), ainsi, on permettra d'accéder à des ressources
d'un serveur situé sur une autre origine que le site courant. 
Pour cela, on utilise donc un middleware [cors](http://expressjs.com/en/resources/middleware/cors.html) qui nous permet 
de résoudre facilement notre probleme. 

Apres un npm install cors, on ajoute une variable dans notre server.js

    const cors = require('cors');
    server.use(cors ({
        origin: 'http://localhost:4200',
        optionSuccessStatus: 200
    }));

![image1](./Assets/Img/Capture%20d’écran%20(80).png)

## _Envoyer des données au serveur avec la méthode POST_


