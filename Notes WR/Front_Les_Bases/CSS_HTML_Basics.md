# ___NOTES : HTML / CSS BASICS___

## ___HTML___

### _Head_

    <meta charset="UTF-8">
    <title>TITRE</title>= Nom de la page affichée dans l'URL

### _Body_ 
On met dans des "tags" body le contenu du site, il se divise souvent en 3 parties principales.

    <header></header>   = Haut de page.
    <content></content> = Millieu de page.
    <footer></footer>   = Bas de pa <*> en existe de nombreuses autres et il peut y avoir plusieurs fois une même structure.


### _Balise de Texte_

    <p>     = Integration de texte.
    <b>     = Bold, permet de mettre le texte en gras.
    <h1>    = Integration de texte dans un titre principal (généralement dans le header).
    <h2>    = Integration de texte dans un titre (en général le plus important autre que le header).
    <a>     = Balise d'ancrage (href = URL du lien & Nom de forme du lien vers lequel rediriger).
    <img>   = Integration d'image (src  = localisation ou URL de l'image & _alt_ = description de l'image pour le
      réferencement).
    <ul>    = Liste textuel non numérotés (li pour ajouter du texte à chaque colonne)
    <ol>    = Liste textuel numérotés (li pour ajouter du texte à chaque colonne)

## ___CSS___

### _Fichier CSS_
New -> Stylesheet -> _CSS_  
Linker le CSS à son HTML (dans head) -> 
      `<link rel="stylesheet" href="/accès au fichier/Nom_du_fichier.css">`

### _Class & ID HTML_
* Donner une __classe__ permet de nommer un groupe et de focus ce groupe plusieurs fois.  
  `.class { }`
* Donner une __ID__ permet de nommer une balise et de la focus (généralement) qu'une seule fois.  
  `#id { }`
* Les __balises__ ont la priorité sur les __class__.
* On peut mettre autant de __class__ qu'on veut sur n'importe quelle type de __balise__.
* On peut rajouter un __!important__ a la suite d'une commande css pour la rendre prioritaire.

### _Organiser son contenu CSS_
Il convient pour une meilleure lisibilité ultérieure ou par des pairs d'organiser son CSS. On retrouvera par exemple en haut de page
les __éléments__ ou __classes__ ayant une utilité dans toute la page. Viennent ensuite les éléments utilisés par ordre d'apparition.  
  
Il est aussi avisé de mettre en commentaire le "nom" de chaque __section__ pour mieux se repérer :   
    `nav` `footer` `main content` par exemple

### _Flexbox_

 _Flexbox est une propriété CSS qui nous permet de gérer simplement la mise en page d'une série d'éléments au  sein d'un élément parent et plus précisément de : Mettre en place plusieurs éléments sur une ligne sans avoir  à se soucier de la largeur de chacun d'eux._

- ### Display
  Permet de définir l'état d'une box\
    `flex` `inline` `block`\
  Il faut toujours commencer par définir son type de `display`.  

- ### Flex-Box commandes
  
    - ### justify-content
      Permet de déplacer les boxes sur l'axe principal (De base horizontal).\
        `flex-start`     : commencer au début de l'axe.\
        `flex-end`       : commencer à la fin de l'axe.\
        `center`         : commencer au milieu de l'axe.\
        `space-around`   : Distancer les boxes et définir un écart autour d'elles.\
        `space-between`  : Distancer les boxes et définir un écart entre elles.\
        `space-evenly`   : Distancer les boxes de façon égale.  
        
    - ### align-items
      Permet de déplacer les box sur l'axe transversal (De base vertical).\
        `flex-start` `flex-end` `center` `baseline` : Les boxes s'alignent à la ligne de base du conteneur.
        `stretch`        : Les boxes sont étirées pour s'adapter au conteneur.  
      
    - ### flex-direction
      Permet de changer la direction d'un axe ou d'inverser les axes.\
        `row`            : Les boxes gardent une structure ligne horizontale.\
        `row-reverse`    : Les boxes prennent une structure ligne horizontale inversée.\
        `column`         : Les boxes prennent une structure verticale.\
        `column-reverse` : Les boxes prennent une structure verticale inversée.\
      
    - ### order
      Permet de définir l'ordre d'une box.
      
    - ### align-self
      Fonctionne de la même manière qu'**align-items**.
      
    - ### flex-wrap
      Permet d'aligner les boxes sur une meme ligne ou sur plusieurs.  
      `nowrap` : Toutes les boxes sont tenues sur une seule ligne horizontale (et donc peuvent se distordre).  
      `wrap` : Les boxes s'étalent sur plusieurs lignes au besoin.  
      `wrap-reverse` : Les boxes s'étalent sur plusieurs lignes dans l'ordre inversé.
      
    - ### flex-flow
        Permet d'utiliser les catégories `flex-wrap` + `flex-direction`
      
    - ### align-content
        Permet de déplacer plusieurs lignes de boites en même temps.\
        `flex-start` `flex-start` `center` `space-between` `space-around` `stretch`

