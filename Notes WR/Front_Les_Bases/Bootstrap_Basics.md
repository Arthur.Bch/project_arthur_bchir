# ___BOOTSTRAP___

## _Installer Bootstrap_

Linker le fichier html avec le css de bootstrap (dans le head) et ajouter le plugin juste avant le closure du body.

Trouvable sur le site de bootstrap :
    https://getbootstrap.com/docs/5.0/getting-started/introduction/

## _Utilisation Bootstrap_

C'est une sorte de librairie possédant de nombreuses fonctions codées en CSS et permettant de simplifier la vie de l'
utilisateur.  
Pour appliquer une commande de _bootstrap_ il faut la mettre en tant que __classe__.  

`<div class="commande" ...>`

## _Grid avec Bootstrap_

X = Taille variable.  
Toujours commencer par le __container__ qui représente une grille entière.\
Puis les __row__ qui sont les rangées du __container__ (axe horizontal).\
Enfin les __col__ représentent les colonnes des lignes (axe vertical).


* ### _Fonctionnement des col_

Sur une ligne il peut y avoir jusqu'à 12 _col_.

Une _col_ de base prend toute la place possible de la ligne.

S'il y a plusieurs _col_ elles se partagent équitablement la place sur la ligne.

Pour définir la taille de la _col_ :
      `<div class="col-X">`

Ne pas appliquer la taille de la _col_ en meme temps qu'une _class_ de base (faites une autre _col_ à
      l'intérieur, dans laquelle vous mettrez la _class_).  

* Les _col_ ont un _margin_ de base entre elles appelés : _gutter_ faisant 30px modifiable avec 
    m/p(_margin_/_padding_)x/y(axe)-X  
    - `<div class="col-4 mx-30">` 
    

* w-100 permet de sauter une ligne.  
* h-100 permet de garder la _col_ sur toutes les lignes de la _row_.  
* Les commandes des _flexbox_ sont applicables sur les _col_ sans display.  
  - `<div class="col-12 justify-content-start"  >`
    

* flex-fill permet de remplir l'espace restant quelle que soit la taille restante.

* ### _Grid Responsive_

Une __grid__ responsive est une __grid__ qui change la taille des éléments selon le Breakpoint(la taille de l'écran).  
Ne pas définir de taille de col en responsive (alors que d'autres tailles le sont) définit la taille xs (mobile).

    xs/auto = .col-X
    s       = .col-sm-X
    m       = .col-md-X
    l       = .col-lg-X
    xl      = .col-xl-X

## _Les composants Bootstrap_

La librairie Boostrap permet d'avoir accès à de nombreux éléments pré-faits appelés "__composants__".  
Ils sont accessibles avec des exemples sur le site officiel de [Boostrap](https://getbootstrap.com/docs/5.0/components/).  
On trouve aussi des ressources utiles dans les parties [Helpers](https://getbootstrap.com/docs/5.0/helpers/clearfix/) ou [Utilities](https://getbootstrap.com/docs/5.0/utilities/api/).