# ___JAVASCRIPT BASICS___


## _Intégrer du code JavaScript dans un fichier HTML_

Il y a deux manières d'ajouter du code JavaScript dans une page :

- linker une page .js :

```html
<script type="text/javascript" src="awsomecode.js"></script>
```

- en ajoutant le code JavaScript à la volée dans le html :

```html
<script type="text/javascript">
    // Mon code Javascript
    ...
</script>
```

## _Les variables et les constantes_

* ### _Les variables_

Une __variable__ est un conteneur servant à stocker des informations de manière __temporaire__, comme une chaine de
caractères (un texte) ou un nombre par exemple. Ex :

```javascript
let myName = "Arthuro";
let myAge = 28;
```

Pour changer la valeur d'une variable il ne faut pas réutiliser la fonction `let`.

* ### _Les constantes_

Une __constante__ est similaire à une variable au sens où c’est également un conteneur pour une valeur. Cependant, à la
différence des variables, on ne va pas pouvoir modifier la valeur d’une constante. Ex :

```javascript
const myConst = "Parapluie";
```

## _Les types de valeurs_

En JavaScript, plusieurs types de valeurs différents existent. On retrouve les suivants :

- Les __string__ (str):

```javascript
let myPanda = "Totoro";
```

- Les __numbers__ (Nbr ou Num):

```javascript
let myPandasNb = 4;
```

- Les __booleans__ (Bool):

```javascript
let vrai = true;
let faux = false;
```

- Les __objects__ (Obj):

```javascript
let myObj = {};
```

## _Les opérateurs arithmétiques_

Les opérateurs arithmétiques vont nous permettre d’effectuer toutes sortes d’opérations mathématiques entre les valeurs
de variables.

- Addition +
- Soustraction –
- Multiplication *
- Division /
- Exponentielle ** (puissance)
- Plus unaire (transforme le type en chiffre)
- Modulo % (reste d’une division euclidienne)

```javascript
let x = 2;
let y = 3;
let z = 4;
let unaire = "4";

x + 1; // 2 + 1 = 3
x + y; // 2 + 3 = 5
x - y; // 2 - 3 = -1
x * y; // 2 * 3 = 6
4 / x; // 4 / 2 = 2
x ** 3; // 2^3 = 2 * 2 * 2 = 8
unaire =+ 4; // converti la string "4" en number 4
5 % 3; // 5 % 3 = 2
```

## _Les opérateurs de comparaison_

Les opérateurs de comparaison __comparent__ deux valeurs et renvoient true (1) ou false (0). Ces telles expressions sont des
valeurs de type __boolean__.

- == Permet de tester l’égalité sur les valeurs (égalité simple)
- === Permet de tester l’égalité en termes de valeurs et de types (égalité stricte)
- != Permet de tester la différence en valeurs (inégalité simple)
- !== Permet de tester la différence en valeurs ou en types (inégalité stricte)
- < Permet de tester si une valeur est strictement inférieure à une autre
- \> Permet de tester si une valeur est strictement supérieure à une autre
- <= Permet de tester si une valeur est inférieure ou égale à une autre
- \>= Permet de tester si une valeur est supérieure ou égale à une autre

```javascript
let nb1 = 2;
let nb2 = 2;
let nb3 = "2";
let nb4 = 3;

nb1 == nb3; // true
nb1 == nb4; // false
nb1 === nb3; // false
nb1 === nb2; // true
nb1 != nb3; // false
nb1 != nb4; // true
nb1 !== nb2; // false
nb1 !== nb3; // true
```

## _Les opérateurs logiques_

Les opérateurs logiques sont des opérateurs qui vont principalement être utilisés pour tester des conditions, ils renvoient 
true ou false.

Les opérateurs logiques vont être représentés par les symboles suivants:

- AND && renvoie true si toutes les comparaisons sont évaluées vraies ou false si ce n'est pas le cas.
- OR || renvoie true si au moins l’une des comparaisons est évaluée comme vrai et false dans le cas contraire.

## _Le cas de l'inverse (!)_  

- ! est un symbole qui permet de renvoyer l'inverse ou l'opposé sur une condition bool, elle sera donc considérée comme 
  false si elle est true et inversement.
- !! renvoie l'inverse de l'inverse, il est utilisé pour transformer une valeur en bool.

## _Les opérateurs d'affectation_

Ces opérateurs permettent de simplifier des opérations en les rendant plus courtes.

- =	Affecte une valeur (à droite) à une variable (à gauche)
- +=	additionne deux valeurs et stocke la somme dans la variable
- -=	soustrait deux valeurs et stocke la différence dans la variable
- *=	multiplie deux valeurs et stocke le produit dans la variable
- /=	divise deux valeurs et stocke le quotient dans la variable
- %=	divise deux valeurs et stocke le reste dans la variable

## _Incrémentation et décrémentation_

Le fait d'incrémenter ou de décrémenter une variable de type num est le fait de lui ajouter ou enlever 1. Le fait de 
placer les signes avant ou après le nombre change le moment de l'incrémentation.

```javascript
let i=0;
i++; i == 1;
++i; i == 2;
i--; i == 1;
--i; i == 0;
```